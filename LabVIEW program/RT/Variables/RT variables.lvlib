﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Control loop" Type="Folder">
		<Item Name="Control.enable" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.init" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.J1.manual.PWM" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">RT Joint manual control.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/RT Joint manual control.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#%A!!!!"5!A!!!!!!$!""!)1NN97ZV97Q`)#B'+1!81!E!%'VB&lt;H6B&lt;#"J&lt;H"V&gt;#!I-#E!!%1!]1!!!!!!!!!"'V*5)%JP;7ZU)'VB&lt;H6B&lt;#"D&lt;WZU=G^M,G.U&lt;!!A1&amp;!!!A!!!!%23D%O&lt;7&amp;O&gt;7&amp;M)'.P&lt;H2S&lt;WQ!!1!#!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.J1.manual.SpeedSP" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">RT Joint manual control.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/RT Joint manual control.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#%A!!!!"5!A!!!!!!$!""!)1NN97ZV97Q`)#B'+1!81!E!%'VB&lt;H6B&lt;#"J&lt;H"V&gt;#!I-#E!!%1!]1!!!!!!!!!"'V*5)%JP;7ZU)'VB&lt;H6B&lt;#"D&lt;WZU=G^M,G.U&lt;!!A1&amp;!!!A!!!!%23D%O&lt;7&amp;O&gt;7&amp;M)'.P&lt;H2S&lt;WQ!!1!#!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.J2.manual.PWM" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">RT Joint manual control.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/RT Joint manual control.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#%A!!!!"5!A!!!!!!$!""!)1NN97ZV97Q`)#B'+1!81!E!%'VB&lt;H6B&lt;#"J&lt;H"V&gt;#!I-#E!!%1!]1!!!!!!!!!"'V*5)%JP;7ZU)'VB&lt;H6B&lt;#"D&lt;WZU=G^M,G.U&lt;!!A1&amp;!!!A!!!!%23D%O&lt;7&amp;O&gt;7&amp;M)'.P&lt;H2S&lt;WQ!!1!#!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.J2.manual.SpeedSP" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">RT Joint manual control.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/RT Joint manual control.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#%A!!!!"5!A!!!!!!$!""!)1NN97ZV97Q`)#B'+1!81!E!%'VB&lt;H6B&lt;#"J&lt;H"V&gt;#!I-#E!!%1!]1!!!!!!!!!"'V*5)%JP;7ZU)'VB&lt;H6B&lt;#"D&lt;WZU=G^M,G.U&lt;!!A1&amp;!!!A!!!!%23D%O&lt;7&amp;O&gt;7&amp;M)'.P&lt;H2S&lt;WQ!!1!#!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.manual" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.setpoints" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:ArrayLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">50</Property>
			<Property Name="Real-Time Features:DatapointsInWaveform" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">True</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">Motor setpoints.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/Motor setpoints.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O;A!!!"5!A!!!!!!$!!^!#1!)5'^T;82J&lt;WY!!!^!#1!)5X2J:GZF=X-!!$1!]1!!!!!!!!!"%UVP&gt;'^S)(.F&gt;("P;7ZU=SZD&gt;'Q!'%"1!!)!!!!"#6.F&gt;("P;7ZU=Q!"!!)!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Control.stop" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
	<Item Name="Encoders" Type="Folder">
		<Item Name="Encoders calibrated" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Encoders initialized" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
	<Item Name="Measurements" Type="Folder">
		<Item Name="Active position" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">Motor setpoints.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/Motor setpoints.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O;A!!!"5!A!!!!!!$!!^!#1!)5'^T;82J&lt;WY!!!^!#1!)5X2J:GZF=X-!!$1!]1!!!!!!!!!"%UVP&gt;'^S)(.F&gt;("P;7ZU=SZD&gt;'Q!'%"1!!)!!!!"#6.F&gt;("P;7ZU=Q!"!!)!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="MotorMeasurements" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:ArrayLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">100</Property>
			<Property Name="Real-Time Features:DatapointsInWaveform" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">True</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">RT Motor display data.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/RT Motor display data.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(Q\!%!!"5!A!!!!!!8!!V!#1!(3D%A9X2S&lt;!!.1!E!"UIS)'.U=GQ!$5!*!!&gt;+-3"N:7&amp;T!!V!#1!(3D)A&lt;76B=Q!.1!E!"UIR)'.V=H)!$5!*!!&gt;+-C"D&gt;8*S!!^!#1!)3D%A=X"F:71!!!^!#1!)3D)A=X"F:71!!!V!#1!'3D%A5&amp;&gt;.!!!.1!E!"EIS)&amp;"841!!#5!*!!."34!!#U!*!!2+&lt;X6U!!!.1!E!"F.I97:U-1!!$5!*!!:4;'&amp;G&gt;$)!!!^!#A!)5'^T;82J&lt;WY!!!F!#1!#4$%!!!F!#1!#4$)!!!^!#1!*5X2J:G:O:8.T!"&amp;!#1!,5'^T;82J&lt;WYA5V!!%U!*!!R4&gt;'FG:GZF=X-A5V!!!!F!#1!#2D%!!!F!#1!#2D)!!'9!]1!!!!!!!!!"'6*5)%VP&gt;'^S)'2J=X"M98EA:'&amp;U93ZD&gt;'Q!2%"1!"9!!!!"!!)!!Q!%!!5!"A!(!!A!#1!+!!M!$!!.!!Y!$Q!1!"%!%A!4!"1!&amp;1R%;8.Q&lt;'&amp;Z)'2B&gt;'%!!!%!&amp;A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Stream.enable" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
	<Item Name="Status" Type="Folder">
		<Item Name="Drive fault clear" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Emergency stop" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Motor 1 status" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">RT Drive FPGA status.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/RT Drive FPGA status.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$JZ1!!!"5!A!!!!!!%!!R!)1:'986M&gt;$]!!'N!&amp;1!'"U6O97*M:71)2'FT97*M:7182'FT97*M:71A9HEA2(*J&gt;G5A2G&amp;V&lt;(182'FT97*M:71A9HEA6'6N=#YA2G&amp;V&lt;(132'FT97*M:71A9HEA23V4&gt;'^Q"V6O;WZP&gt;WY!"F.U982V=Q!!(%!B&amp;E^W:8)A6'6N='6S982V=G5A2G&amp;V&lt;(1!!$]!]1!!!!!!!!!"'&amp;*5)%2S;8:F)%:12U%A=X2B&gt;(6T,G.U&lt;!!?1&amp;!!!Q!!!!%!!AR%=GFW:3"T&gt;'&amp;U&gt;8-!!!%!!Q!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Motor 2 status" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typedefName1" Type="Str">RT Drive FPGA status.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Typedefs/RT Drive FPGA status.ctl</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$JZ1!!!"5!A!!!!!!%!!R!)1:'986M&gt;$]!!'N!&amp;1!'"U6O97*M:71)2'FT97*M:7182'FT97*M:71A9HEA2(*J&gt;G5A2G&amp;V&lt;(182'FT97*M:71A9HEA6'6N=#YA2G&amp;V&lt;(132'FT97*M:71A9HEA23V4&gt;'^Q"V6O;WZP&gt;WY!"F.U982V=Q!!(%!B&amp;E^W:8)A6'6N='6S982V=G5A2G&amp;V&lt;(1!!$]!]1!!!!!!!!!"'&amp;*5)%2S;8:F)%:12U%A=X2B&gt;(6T,G.U&lt;!!?1&amp;!!!Q!!!!%!!AR%=GFW:3"T&gt;'&amp;U&gt;8-!!!%!!Q!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Overcurrent" Type="Variable">
			<Property Name="featurePacks" Type="Str">Global,Real-Time Features</Property>
			<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
			<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
			<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
			<Property Name="type" Type="Str">Global</Property>
			<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
	<Item Name="Controller FGV.vi" Type="VI" URL="../Controller FGV.vi"/>
	<Item Name="RT globals.vi" Type="VI" URL="../RT globals.vi"/>
	<Item Name="RT.stop" Type="Variable">
		<Property Name="Description:Description" Type="Str">VAriable for stopping whole appication</Property>
		<Property Name="featurePacks" Type="Str">Description,Global,Real-Time Features</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
		<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
		<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
